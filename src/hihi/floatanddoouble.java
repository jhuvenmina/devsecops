package hihi;

import java.util.Scanner;

public class floatanddoouble {

    public static void main (String []args){


        float myminfloat = Float.MIN_VALUE;
        float mymaxfloat = Float.MAX_VALUE;

        System.out.println("My min float value:" + myminfloat);
        System.out.println("My max float value:" + mymaxfloat);

        double mymindouble = Double.MIN_VALUE;
        double mymaxdouble = Double.MAX_VALUE;

        System.out.println("My min double value:" + mymindouble);
        System.out.println("My max double value:" + mymaxdouble);


        int myint = 5 % 2;
        float myfloat = 5f / 3f;
        double mydouble = 5d / 3d;

        System.out.println(myint);
        System.out.println(myfloat);
        System.out.println(mydouble);


        double pounds;
        double kilogram;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter your pounds:");
        pounds = in.nextDouble();

        kilogram = pounds * 0.45359237;

        System.out.println("your kilograms is:" + kilogram);


    }
}
