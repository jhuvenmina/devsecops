package hihi;

public class palindrome {

    public static void main(String[] args){

        System.out.println(sumFirstAndLastDigit(11));

        System.out.println(isPalindrome(1002));
    }
    public static boolean isPalindrome(int number){
        int reversedInteger = 0;
        int original = number;
        while( number != 0 )
        {

            int remainder = number % 10;
            reversedInteger = reversedInteger * 10 + remainder;
            number /= 10;


        }
        if (original == reversedInteger){
            return true;
        }

        return false;
    }



    public static int sumFirstAndLastDigit(int num) {
        if (num < 0) {
            return -1;
        }
        int firstD = num;

        for (int i = num; i > 0; i /= 10) {
            firstD = i;
        }

        return firstD + (num % 10);
    }
}
