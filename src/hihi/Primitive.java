package hihi;

public class Primitive {

    public static void main (String []args){

        int myvalue = 10000;
        int myminintvalue = Integer.MIN_VALUE;
        int mymaxintvalue = Integer.MAX_VALUE;

        System.out.println("Integer Minimum Value:" + myminintvalue);
        System.out.println("Integer Maximum Value:" + mymaxintvalue);

        short myminshort = Short.MIN_VALUE;
        short mymaxshort = Short.MAX_VALUE;
        System.out.println(myminshort);
        System.out.println(mymaxshort);

        byte myminbyte = Byte.MIN_VALUE;
        byte mymaxbyte = Byte.MAX_VALUE;
        System.out.println(myminbyte);
        System.out.println(mymaxbyte);

        long mylongvalue = 100;
        long myminlong = Long.MIN_VALUE;
        long mymaxlong = Long.MAX_VALUE;
        System.out.println(myminlong);
        System.out.println(mymaxlong);
        long biglongliteral = 214748364731232L;
        System.out.println(biglongliteral);

        byte hihi = 10;

        short hihi3 = 20;

        int hihi6 = 50;

        long hihi2 = 50000L + (10L*(hihi+hihi3+hihi6));

        System.out.println(hihi2);


    }
}
