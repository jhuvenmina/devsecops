package hihi;

public class secondsminutes {
    private static final String INVALID_VALUE_MESSAGE = "Invalid Number";

    public static void main (String[] args){

        getDurationString(61,3000 );
      //  getDurationString(3945);
    }

    public static int getDurationString(int minutes, int seconds){
        if(minutes >= 0 &&(seconds >= 0 && seconds <= 59)){

            int hours = minutes/60;
            int remainingminutes = minutes%60;


            System.out.println(hours +"hr " +remainingminutes + "m " +seconds+"s");

        }
        System.out.println(INVALID_VALUE_MESSAGE);
        return -1;
    }

    public static int getDurationString(int seconds){
        if(seconds >=0){

            int minutes = seconds/60;
            int remainingseconds = seconds%60;

            return getDurationString(minutes,remainingseconds);
        }

        return - 1;

    }
}
