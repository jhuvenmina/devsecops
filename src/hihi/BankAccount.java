package hihi;

public class BankAccount {


    private int accountNumber;
    private int balance;
    private String customerName;
    private String email;
    private String mobileNo;


    public BankAccount(String customerName, String email, String mobileNo) {
        this(00000123564,2000001,customerName,email,mobileNo);
        this.customerName = customerName;
        this.email = email;
        this.mobileNo = mobileNo;
    }

    public BankAccount(){
        System.out.println("CALLED");
    }
    public BankAccount(int accountNumber, int balance, String customerName, String email, String mobileno){

        setBalance(balance);
        this.accountNumber = accountNumber;
        this.balance = balance;
        this.customerName = customerName;
        this.email = email;
        this.mobileNo = mobileno;

    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(int accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public void deposit(int amount){

        this.balance += amount;
        System.out.println("Deposit successful, New balance is: "+this.balance);
        
    }

    public void withdraw (int amount){
        if (this.balance-amount < 0){
            System.out.println("Only "+balance+ " available");
        }
        else{
            this.balance -= amount;
            System.out.println(+amount+" successfully withdrawn, remaining balance is: "+this.balance);
        }

    }



}
