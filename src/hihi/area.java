package hihi;

public class area {
    public static void main(String[] args){

area(5.0);
area(4,5);
    }


    public static double area(double radius){

        if(radius >= 0){

            double area = (radius*radius)*Math.PI;
            return area;

        }
        return -1.0;
    }

    public static double area(double x, double y){

        if(x >= 0 && y >= 0){

            double area = (x*y);
            return area;

        }
        return -1.0;
    }

}
